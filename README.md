# Ruby on Rails Tutorial sample application
This is the sample application for implement Continous Integration

## License
All source code in the [Ruby on Rails Tutorial](http://railstutorial.org/)
is available jointly under the MIT License and the Beerware License. See
[LICENSE.md](LICENSE.md) for details.

## Getting started
Ruby version

```
jruby 9.1.6.0
```

Install the needed gems:

```
$ bundle install
```

Next, migrate the database:

```
$ bundle exec rails db:migrate
```

Finally, run the test suite to verify that everything is working correctly:

```
$ bundle exec rspec
```

If the test suite passes, you'll be ready to run the app in a local server:

```
$ bundle exec rails s
```

Using a browser, go to 
```
http://localhost:3000
```
and you'll see: "Hello, Rails!"

